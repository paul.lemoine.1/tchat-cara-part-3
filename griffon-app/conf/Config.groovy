application {
    title = 'app'
    startupGroups = ['app']
    autoShutdown = true
}
mvcGroups {
    // MVC Group for "app"
    'app' {
        model      = 'tchat.AppModel'
        view       = 'tchat.AppView'
        controller = 'tchat.AppController'
    }
	'tchat' {
		model      = 'tchat.TchatModel'
		view       = 'tchat.TchatView'
		controller = 'tchat.TchatController'
	}
	// MVC Group for "ip"
	//TO-DO
	
	// MVC Group for "tchatIp"
	//TO-DO
	
}