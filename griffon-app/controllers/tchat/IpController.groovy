package tchat

import javax.annotation.Nonnull

import griffon.core.artifact.GriffonController
import griffon.core.controller.ControllerAction
import griffon.inject.MVCMember
import griffon.metadata.ArtifactProviderFor
import griffon.transform.Threading

@ArtifactProviderFor(GriffonController)
class IpController {
	@MVCMember @Nonnull
	def model
	@MVCMember @Nonnull
	def view
	
	void mvcGroupInit(Map args) {

	}
	
	@ControllerAction
	@Threading(Threading.Policy.INSIDE_UITHREAD_ASYNC)
	void launchTchat() {
		//TO-DO
	}
}
