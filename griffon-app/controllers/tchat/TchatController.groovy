package tchat

import java.util.logging.Logger

import javax.annotation.Nonnull

import griffon.core.artifact.GriffonController
import griffon.core.controller.ControllerAction
import griffon.inject.MVCMember
import griffon.metadata.ArtifactProviderFor
import griffon.transform.Threading
import javafx.scene.control.Label

@ArtifactProviderFor(GriffonController)
class TchatController {
	@MVCMember @Nonnull
	def model
	@MVCMember @Nonnull
	def view
	
	@javax.inject.Inject
	private ServerService servService
	
	def cmptY
	def ip

	void mvcGroupInit(Map args) {
		model.pseudo = args.pseudo
		Logger logger = Logger.getLogger("")
		logger.info("le pseudo est :" +model.pseudo)
		ip ="localhost"
		cmptY=0
		enbableConnection(ip)
		receiveMessage()
	}

	@Threading(Threading.Policy.OUTSIDE_UITHREAD)
	void enbableConnection(def ip){
		servService.enableConnection(ip)
	}

	@Threading(Threading.Policy.INSIDE_UITHREAD_ASYNC)
	void sendMessage() {
		Logger logger = Logger.getLogger("")
		logger.info ("I am a test info log")
		logger.info("le message est :" +model.message)
		String messSend = model.pseudo +" : "+ model.message
		servService.sendMessage(messSend)
		view.builder.textSend.text=""
	}

	@Threading(Threading.Policy.OUTSIDE_UITHREAD_ASYNC)
	@ControllerAction
	void receiveMessage() {
		servService.receiveMessage()
		addMessage()
		cmptY++
	}

	@Threading(Threading.Policy.OUTSIDE_UITHREAD_ASYNC)
	void addMessage() {
		runOutsideUI{
			while(true) {
				String messReceive=""
				if(!servService.isEmpty()) {
					try {
						messReceive =servService.getMessage()
					}finally{
						runInsideUIAsync {
							view.builder.gridTchat.add(new Label(messReceive),0,cmptY)
						}
					}
				}else {
					sleep(1000)
				}
				cmptY++
			}
		}
	}

	@ControllerAction
	@Threading(Threading.Policy.INSIDE_UITHREAD_ASYNC)
	public void quit() {
		servService.closeConnection()
		getApplication().shutdown()
	}
}
