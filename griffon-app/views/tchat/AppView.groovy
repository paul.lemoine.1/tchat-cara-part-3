package tchat

import griffon.core.artifact.GriffonView
import griffon.inject.MVCMember
import griffon.metadata.ArtifactProviderFor
import javafx.scene.control.TabPane
import org.codehaus.griffon.runtime.javafx.artifact.AbstractJavaFXGriffonView
import javax.annotation.Nonnull

@ArtifactProviderFor(GriffonView)
class AppView extends AbstractJavaFXGriffonView {
    @MVCMember @Nonnull
    def builder
    @MVCMember @Nonnull
    def controller
    @MVCMember @Nonnull
    def model
	

    void initUI() {
		builder.with{
			stage(id:'stageMenu', title: 'Tchat', visible: true) {
				
				scene(id: 'scene', fill: WHITE, width: 400, height: 600) {
					
					g = gridPane(id:'gridMenu', hgap: 5,vgap: 10, padding:25, alignment:'top_center') {
						label(row: 0, column: 0, text: 'Entrez votre pseudo :')
						text = textField(id:'fieldPseudo', row: 1, column: 0, promptText: 'Type here', prefColumnCount: 80, text: bind(model.pseudoProperty()))
						button(row: 2, column: 0, prefWidth: 200,
							griffonActionId: 'launchTchat', id: 'Valider')
						//TO-DO
						button(row: 4, column: 0, prefWidth: 200,
						       griffonActionId: 'quit', id: 'Quitter')
					}
					connectActions(g, controller)
					connectMessageSource(g)
				}
			}
		}
		builder.Valider.text="Valider"
		//TO-DO
		builder.Quitter.text="Quitter"
    }
}