package tchat

import javax.annotation.Nonnull

import org.codehaus.griffon.runtime.javafx.artifact.AbstractJavaFXGriffonView

import griffon.core.artifact.GriffonView
import griffon.inject.MVCMember
import griffon.metadata.ArtifactProviderFor

@ArtifactProviderFor(GriffonView)
class TchatView extends AbstractJavaFXGriffonView {
	@MVCMember @Nonnull
	def builder
	@MVCMember @Nonnull
	def controller
	@MVCMember @Nonnull
	def model
	@MVCMember @Nonnull
	def parentView


	@Override
	public void initUI() {
		builder.with{
			scene(id: 'tchat', fill: WHITE, width: 780, height: 600) {
				hbox{
					group{
						gridPane(id:'gridMenuTchat',style:'-fx-background-color:grey;'){
							scrollPane(row: 0, column: 0, minHeight: 575) {
								gridPane(id:'gridTchat', alignment:'top_left', style:'-fx-background-color:grey;'){
									scale(x:2,y:2)
								}
							}
							gridMenu = gridPane(id:'gridMenuTchat',style:'-fx-background-color:grey;', row: 1, column: 0){
								
								textField(id:'textSend', row: 0, column: 0, promptText: 'Entrez votre message...', prefColumnCount: 52, text: bind(model.messageProperty()))
								button(row: 0, column: 1, prefWidth: 100, griffonActionId: 'sendMessage', id: 'Envoyer')
							}
						}
					}
				}

				connectActions(gridMenu, controller)
				connectMessageSource(gridMenu)
			}
		}
		builder.Envoyer.text="Envoyer"
		
		parentView.builder.stageMenu.scene=builder.tchat
	}
}
