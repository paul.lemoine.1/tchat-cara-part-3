package tchat

import griffon.core.artifact.GriffonModel
import griffon.metadata.ArtifactProviderFor
import griffon.transform.FXObservable
import griffon.transform.Observable
import groovyx.javafx.beans.FXBindable

@ArtifactProviderFor(GriffonModel)
class TchatModel {
	@FXObservable String pseudo= ""
	@FXObservable String message= ""
}
