# Partie 3 : In local~~host~~ we trust !

- la correction de la partie 2 est présente dans cette même partie n'hésitez pas à y jeter un œil !
- Une fois la seconde partie faite, nous allons nous occuper de la partie d'interconnexion entre les utilisateurs.
- Avant de démarrer, nous avons mis a disposition un autre serveur permettant la communication des messages :
	- lien à cloner : https://gitlab.com/paul.lemoine.1/serveur_tchat_cara.git
	- A importer dans Eclipse comme un simple projet java.
- A la fin de cette partie vous arriverez a ce résultat :

![alt text](https://imageshack.com/a/img923/5771/5pnYGW.png  "Resultat partie 3")

Puis la fenêtre de selection de l'ip : 

![alt text](https://imageshack.com/a/img922/1315/TZnzAL.png  "Resultat partie 3 ip")

# 1. Configuration du fichier Config.groovy
- Les classes dont vous avez besoin pour réaliser cette partie son déjà présentes.
-  2 MVC on été ajoutés : le MVC "ip" et "tchatIp"
- Déclarez le nom 'ip' du MVCGroup ainsi que les classes associées.
	- Nous avons :
		- le IpController
		- le IpModel
		- le IpView
- Déclarez le nom 'tchatIp' du MVCGroup ainsi que les classes associées.
	- Nous avons :
		- le TchatIpController
		- le TchatIpModel
		- le TchatIpView

# 2. Partie MVC Ip
## 2.1  La IpView :
- Dans cette partie vous devez créer la vue permettant la selection de l'ip (pour vous aider vous pouvez vous baser sur l'AppView)
- Notre view se compose de :
	-  le builder
		- la scene : 
			- id: ip
			- un gridPane
				- id: gridMenu
				- un label pour le pseudo
				- un textField
					- id: fieldPseudo
				- un label pour l'ip
				- un textField
					- id: fieldIp
				- un boutton
					- id : Valider
					- méthode du controller : launchTchat	()
-  Associez le gridMenu au controller.
-  Associez le texte du bouton Valider.
- Faire le changement de vue en plaçant la scène du builder de la view dans le scène du builder de sa parentView.

## 2.2 Le IpModel:
- Dans cette partie nous allons juste définir ce que doit stocker la vue, ici nous avons besoin de :
	- le pseudo de l'utilisateur .
	- l'ip entrez par l'utilisateur.

- Une fois cela fait, retounez dans la vue est "bindez" les messages sur les textFields de la vue.

## 2.3 Le IpController :

- Dans cette partie nous allons nous occuper de la création du MVC tchatIp.

### 2.3.1 Les méthodes :

#### 2.3.1.1 Implémentation de la méthode launchTchat()
- Cette méthode vérifie juste si le pseudo et l'ip ne sont pas vides et créé le MVC de tchatIp.
- Instanciez le MVC tchatIp avec en paramètre le pseudo et l'ip.

## 2.4 AppView
- pour instancier notre vue nous devons ajouter un boutton "Ip" bindé sur la méthode launchWithIp de l'AppController.

## 2.5 AppController
- Dans la méthode launchWithIp() nous instancierons le groupMVC ip.


# 3. Partie MVC tchatIp

## 3.1  La TchatIpView :
- Cette view est un copié-collé de notre TchatView en y intégrant un stage en plus qui a pour id 'tchat ip'. 
- Par la présence de ce stage nous n'avons pas besoin de passer notre scène à notre parentView vu que nous crééons une nouvelle fenêtre.

## 3.2 Le TchatIpModel:
- Dans cette partie nous allons juste définir ce que doit stocker la vue, ici nous avons besoin de :
	- le pseudo de l'utilisateur .
	- le message que l'on veut envoyer.

- Une fois cela fait, retournez dans la vue et "bindez" le message sur le textField de la vue.

## 3.3 Le TchatIpController :

- Dans cette partie nous allons nous occuper de la gestion des messages ainsi que la communication avec le service de tchat.
- Dans la méthode, le service est déjà injecté ainsi que les variables dont vous aurez besoin sont définis.

### 3.3.1 Les méthodes :

#### 3.3.1.1 Implémentation de la méthode MVCGroupInit()
- Cette méthode est un copié-collé de notre méthode dans le TchatController à une seule différence, nous n'avons pas besoin d'affecter notre ip en dur.

	- Initialisation des variables: 
		- La récupération du pseudo et de l'ip se fait via le paramètre  :Map args.
		- Le compteurY à 0
	- lancement des méthodes :
		- appelez la méthode enableConnection qui prend en paramètre l'ip passé en paramètre via la Map args
		- et la méthode receiveMessage qui permet la récupération des messages.

# 4. C'est prêt !
- Une fois que vous êtes arrivez ici l'application est terminée, vous pouvez alors soit restez en local sur votre pc ou en donnant votre ip à vos collègues pour qu'ils se connectent sur votre serveur de tchat ! 

- Vous trouverez votre ip local dans la liste des ip affichées par le serveur lors de son initialisation!

